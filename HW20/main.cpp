#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

using namespace std;

enum   LuckyDay
{
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
	SUNDAY
};

LuckyDay GetLuckyDay(int td)
{ 
	 return static_cast<LuckyDay>(td);
}

struct SkillTree
{
	int Str;
	int Agi;
	int Int;
	int Luc;
};
struct SamIAm
{ 
	int MyAge;
	LuckyDay MyLuckyDay;
	string MyName; 
	SkillTree MySkillTree;

	LuckyDay GetLuckyDay()
	{
		return MyLuckyDay;
	}
};

int main()
{
	SamIAm* ptrSam = new SamIAm{ 99, static_cast<LuckyDay>(2), "Sam", SkillTree{4,5,9,999} };

	time_t now = time(0);
	tm* ltm = localtime(&now);
	if(GetLuckyDay(ltm->tm_wday) == ptrSam->GetLuckyDay())
	{
		std::cout << "Moo! Today is Sam's Lucky day!\n";
		std::cout << "Sam's Luck is: " << ptrSam->MySkillTree.Luc;
	}
	return 0;
}